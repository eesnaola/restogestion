<?php
namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Client;
use App\Entity\License;

class ClientAdminController extends BaseAdminController
{
    public function preUpdateClientEntity($client)
    {
       $em = $this->getDoctrine()->getManager();
       $dbClient = $em->getUnitOfWork()->getOriginalEntityData($client);
       if ($dbClient['expiration'] != $client->getExpiration()){
          $licenses = $em->getRepository(License::class)->findByClient($client);
          $client->setFreeLicenses($client->getFreeLicenses() + count($licenses));
          foreach ($licenses as $license) {
            $em->remove($license);
          }
          $em->flush();
       }
    }

}
