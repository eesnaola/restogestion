<?php
namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Client;
use App\Entity\License;

class LicenseAdminController extends BaseAdminController
{
    public function prePersistLicenseEntity($license)
    {
      $fechaH = dechex((round(25569+($license->getClient()->getExpiration()->getTimestamp()/86400))) / 2);
      if ($fechaH == "0")
          $fechaH = "0000";

      $sHash = substr(md5(strtoupper($fechaH.str_replace("-","",$license->getSerial()).$license->getClient()->getProduct())), 0, 12);
      $hash = $sHash.$fechaH."AB".$license->getClient()->getFeatures();
      $cdKey = strtoupper(substr($hash, 0, 4).'-'.substr($hash, 4, 4).'-'.substr($hash, 8, 4).'-'.substr($hash, 12, 4).'-'.substr($hash, 16, 4));

      $license->setCdKey($cdKey);
      $license->setLastAccess(new \DateTime(date('Y-m-d h:i:s')));
    }

}
