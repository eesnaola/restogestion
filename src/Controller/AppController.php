<?php
namespace App\Controller;

use App\Entity\Client;
use App\Entity\License;
use App\Entity\Audit;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;


use Symfony\Component\HttpFoundation\RedirectResponse;

class AppController extends Controller
{
    /**
     * @Route("/UpdateChecker/hayconexion.php")
     * @Method("POST")
     */
    public function indexAction(Request $request): Response
    {
        $licenseUtil = $this->get('app.license_util');
        $data = $request->request->get('DATA');
        $serial = substr($data, 26);
        switch(substr($data,0,26)) {
            case "FINGERPASS.TESTCONNECTION=":
                $result = $licenseUtil->validateLicense($serial, $license);
                switch($result) {
                    case 1:
                        $now = new \DateTime();
                        $daysRemaining = $now->diff($license->getClient()->getExpiration());
                        $expirationEx = dechex(round(25569+($license->getClient()->getExpiration()->getTimestamp()/86400)));
                        $response = "SERVEROK|".$license->getSerial()."|".$expirationEx."|".$license->getCdKey()."|".$daysRemaining->format('%R%a')."|";
                    break;
                    case 2:
                        $response = "LICENSEEXPIRED|||";
                    break;
                    default:
                        $response = "INVALIDLICENSE|||";
                    break;
                }
            break;
            case "FINGERPASS.ACTIVATION....=":
                $dat = explode("|",$serial);
                $response = $licenseUtil->newLicense($dat[0], $dat[1], $dat[2], $dat[3]);
            break;
            default:
                $response = "ERROR NO CODE GIVEN";
            break;
        }
        return new Response($response);
    }

    /**
     * @Route("/home/uploads/uploadAuditoria/{id}/")
     * @Method("POST")
     */
    // public function auditAction(Request $request, $id): Response
    // {
    //     $data = $request->request->get('DATA');
    //     $em = $this->getDoctrine()->getManager();
    //     $license = $em->getRepository(License::class)->findOneBySerial($id);
    //
    //     if ($license) {
    //       $audit = new Audit();
    //       $audit->setLicense($license);
    //       $audit->setData($data);
    //       $em->persist($audit);
    //       $em->flush();
    //     }
    //
    //     $response = "ok";
    //     return new Response($response);
    // }


    /**
     * @Route("/test")
     * @Method("GET")
     */
    //     public function testAction()
    //     {
    // return new RedirectResponse('http://google.com');
    //     }
}
