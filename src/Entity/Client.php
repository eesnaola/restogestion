<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="client")
 */
class Client
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $userName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $password;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $plainPassword;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $features;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $product;

    /**
     * @ORM\Column(type="date")
     */
    public $expiration;

    /**
     * @ORM\Column(type="integer")
     */
    public $freeLicenses;

    /**
     * @ORM\OneToMany(targetEntity="License", mappedBy="client")
     */
    private $licences;

    /**
     * Constructor
     */
    public function __construct()
    {
      $now = new \DateTime(date('Y-m-d h:i:s'));
      $this->features = "07";
      $this->product = "R1";
      $this->freeLicenses = 1;
      $this->plainPassword = "1234";
      $this->expiration = $now->add(new \DateInterval('P30D'));

    }

    public function __toString()
    {
        return $this->getUserName();
    }

    /**
     * Set the value of Plain Password
     *
     * @param mixed plainPassword
     *
     * @return self
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        $this->password = md5($plainPassword);
        return $this;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of User Name
     *
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set the value of User Name
     *
     * @param mixed userName
     *
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get the value of Password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of Password
     *
     * @param mixed password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of Plain Password
     *
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Get the value of Features
     *
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Set the value of Features
     *
     * @param mixed features
     *
     * @return self
     */
    public function setFeatures($features)
    {
        $this->features = $features;

        return $this;
    }

    /**
     * Get the value of Product
     *
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set the value of Product
     *
     * @param mixed product
     *
     * @return self
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get the value of Expiration
     *
     * @return mixed
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * Set the value of Expiration
     *
     * @param mixed expiration
     *
     * @return self
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;

        return $this;
    }

    /**
     * Get the value of Free Licenses
     *
     * @return mixed
     */
    public function getFreeLicenses()
    {
        return $this->freeLicenses;
    }

    /**
     * Set the value of Free Licenses
     *
     * @param mixed freeLicenses
     *
     * @return self
     */
    public function setFreeLicenses($freeLicenses)
    {
        $this->freeLicenses = $freeLicenses;

        return $this;
    }

    /**
     * Get the value of Licences
     *
     * @return mixed
     */
    public function getLicences()
    {
        return $this->licences;
    }

    /**
     * Set the value of Licences
     *
     * @param mixed licences
     *
     * @return self
     */
    public function setLicences($licences)
    {
        $this->licences = $licences;

        return $this;
    }

}
