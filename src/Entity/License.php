<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="license")
 */
class License
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $serial;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $cdKey;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="licenses")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\Column(type="datetime")
     */
    public $lastAccess;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $lastAccessExpiration;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $lastIp;

    /**
     * @ORM\OneToMany(targetEntity="Audit", mappedBy="license")
     */
    private $udits;


    public function __toString()
    {
        return $this->getSerial();
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Serial
     *
     * @return mixed
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set the value of Serial
     *
     * @param mixed serial
     *
     * @return self
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get the value of Cd Key
     *
     * @return mixed
     */
    public function getCdKey()
    {
        return $this->cdKey;
    }

    /**
     * Set the value of Cd Key
     *
     * @param mixed cdKey
     *
     * @return self
     */
    public function setCdKey($cdKey)
    {
        $this->cdKey = $cdKey;

        return $this;
    }

    /**
     * Get the value of Client
     *
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set the value of Client
     *
     * @param mixed client
     *
     * @return self
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get the value of Last Access
     *
     * @return mixed
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * Set the value of Last Access
     *
     * @param mixed lastAccess
     *
     * @return self
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }

    /**
     * Get the value of Last Access Expiration
     *
     * @return mixed
     */
    public function getLastAccessExpiration()
    {
        return $this->lastAccessExpiration;
    }

    /**
     * Set the value of Last Access Expiration
     *
     * @param mixed lastAccessExpiration
     *
     * @return self
     */
    public function setLastAccessExpiration($lastAccessExpiration)
    {
        $this->lastAccessExpiration = $lastAccessExpiration;

        return $this;
    }

    /**
     * Get the value of Last Ip
     *
     * @return mixed
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * Set the value of Last Ip
     *
     * @param mixed lastIp
     *
     * @return self
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;

        return $this;
    }


    /**
     * Get the value of Udits
     *
     * @return mixed
     */
    public function getUdits()
    {
        return $this->udits;
    }

    /**
     * Set the value of Udits
     *
     * @param mixed udits
     *
     * @return self
     */
    public function setUdits($udits)
    {
        $this->udits = $udits;

        return $this;
    }

}
