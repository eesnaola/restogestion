<?php
namespace App\Utils;

use App\Entity\Client;
use App\Entity\License;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;

class LicenseUtil {

  protected $em;
  protected $now;

  public function __construct(EntityManager $em) {
    $this->em = $em;
    $this->nowdiff = new \DateTime(date('Y-m-d'));
    $this->now = new \DateTime(date('Y-m-d h:i:s'));
  }

  public function newLicense($clientSerial, $userName, $password, $product) {
      $client = $this->em->getRepository(Client::class)->findOneBy(array('userName' => $userName, 'password' => $password));

      if (!$client)
          return "ACT:ERROR|USERNOTVALID|";

      if ($product != $client->getProduct())
          return "ACT:ERROR|PRODUCTMISMATCH|";

      $expiration = intval($this->now->diff($client->getExpiration())->format("%R%a"));

      if($expiration < 0)
          return "ACT:ERROR|USERLICENSEEXPIRED|";

      $license = $this->em->getRepository(License::class)->findOneBy(array('serial' => $clientSerial, 'client' => $client));
          if ($license)
          return "ACT:OK|".$license->getCdKey()."|";

      if ($client->getFreeLicenses() <= 0)
          return "ACT:ERROR|NOMORELICENSES|";

      $license = new License();
      $license->setClient($client);
      $license->setSerial($clientSerial);

      $fechaH = dechex((round(25569+($client->getExpiration()->getTimestamp()/86400))) / 2);
      if ($fechaH == "0")
          $fechaH = "0000";

      $sHash = substr(md5(strtoupper($fechaH.str_replace("-","",$clientSerial).$product)), 0, 12);
      $hash = $sHash.$fechaH."AB".$client->getFeatures();
      $cdKey = strtoupper(substr($hash, 0, 4).'-'.substr($hash, 4, 4).'-'.substr($hash, 8, 4).'-'.substr($hash, 12, 4).'-'.substr($hash, 16, 4));


      $license->setCdKey($cdKey);
      $license->setLastAccess($this->now);
      $license->setLastIp($_SERVER['REMOTE_ADDR']);
      $this->em->persist($license);
      $client->setFreeLicenses($client->getFreeLicenses() - 1);
      $this->em->persist($client);
      $this->em->flush();
      return "ACT:OK|".$cdKey."|";
  }

  public function validateLicense($serial, &$license) {
      $license = $this->em->getRepository(License::class)->findOneBy(array('serial' => $serial));
      if (!$license)
          return 0;

      $expiration = intval($this->nowdiff->diff($license->getClient()->getExpiration())->format("%R%a"));

      if($expiration <= 0){
          $license->setLastAccessExpiration($this->now);
          $license->setLastIp($_SERVER['REMOTE_ADDR']);
          $this->em->persist($license);
          $this->em->flush();
          return 2;
      }
      $license->setLastAccess($this->now);
      $license->setLastIp($_SERVER['REMOTE_ADDR']);
      $this->em->persist($license);
      $this->em->flush();
      return 1;
  }
}
